#!/usr/bin/env python3

import argparse
import logging
import sys
from ecalautoctrl.processing_ctrl import sync_runs, reprocess_runs, create_campaign, close_processing, list_wflows, create_rtype, update_wflows, status, list_campaigns, inject_campaign, inject_campaign_mc
from ecalautoctrl.notifications import MattermostHandler


def cmdline_options():
    """
    Return the argparse instance to handle the cmdline options for this script.

    The function is needed by sphinx-argparse to easily generate the docs.
    """
    parser = argparse.ArgumentParser(description="""
    Fetch information from OMS and create a corresponding entry
    in the ECAL influxdb
    """, formatter_class=argparse.RawTextHelpFormatter)

    parser.add_argument('--db',
                        dest='dbname',
                        default='ecal_online_test',
                        type=str,
                        help='Database name, default is test db')
    parser.add_argument('--notify',
                        dest='notify',
                        default='',
                        type=str,
                        help='Mattermost incoming webhook url for notifications')

    def add_campaign_opt(parser):
        parser.add_argument('-c', '--campaign',
                            dest='campaign',
                            default='prompt',
                            required=True,
                            type=str,
                            help='Processing campaign')
    
    # sub commands
    subparsers = parser.add_subparsers(dest='subcommand', description='Select operation')

    # reprocess subcommand
    reprocess_parser = subparsers.add_parser('reprocess', help='Inject a specific list of runs in "reprocess" state')
    reprocess_parser.set_defaults(func=reprocess_runs)
    add_campaign_opt(reprocess_parser)
    reprocess_parser.add_argument('--tasks',
                                  dest='tasks',
                                  default=None,
                                  type=str,
                                  nargs='+',
                                  help='Which workflows to replay, space separated, default all')
    reprocess_rlist = reprocess_parser.add_mutually_exclusive_group(required=True)
    reprocess_rlist.add_argument('--runs',
                                 dest='runs',
                                 default='',
                                 type=lambda opt : opt.split(',') if opt else None,
                                 help='Comma separated list of run(s) to reprocess.')
    reprocess_rlist.add_argument('--fills',
                                 dest='fills',
                                 default='',
                                 type=lambda opt: opt.split(',') if opt else None,
                                 help='Comma separated list of fill(s) to reprocess.')
    reprocess_rlist.add_argument('--range',
                                 dest='rrange',
                                 default='',
                                 type=lambda opt: opt.split(',') if opt else [None, None],
                                 help='Define a range of runs to be reprocessed (min,max).')
    reprocess_parser.add_argument('--era',
                                  dest='era',
                                  default=None,
                                  type=str,
                                  help='Override CMS acquisition era information.')
    reprocess_parser.add_argument('--globaltag',
                                  dest='globaltag',
                                  default=None,
                                  type=str,
                                  help='Override GT.')
    
    # create subcommand
    # options to setup a new reprocessing campaign
    create_parser = subparsers.add_parser('create', help='Create a new reprocessing campaign')
    create_parser.set_defaults(func=create_campaign)
    add_campaign_opt(create_parser)
    create_parser.add_argument('--clone',
                               dest='clone',
                               default=None,
                               type=str,
                               help='An existing campaign to clone.') 
    # inject subcommand
    # options to setup a new reprocessing campaign
    inject_parser = subparsers.add_parser('inject', help='Inject new data in a campaign')
    inject_parser.set_defaults(func=inject_campaign)
    add_campaign_opt(inject_parser)
    inject_rlist = inject_parser.add_mutually_exclusive_group(required=False)
    inject_rlist.add_argument('--runs',
                              dest='runs',
                              default='',
                              type=lambda opt: opt.split(',') if opt else None,
                              help='Comma separated list of run(s) to inject in the new campaign.')
    inject_rlist.add_argument('--range',
                              dest='rrange',
                              default='',
                              type=lambda opt: opt.split(',') if opt else [None, None],
                              help='Define a range of runs to be injected (min,max).')
    inject_rlist.add_argument('--fills',
                              dest='fills',
                              default='',
                              type=lambda opt: opt.split(',') if opt else None,
                              help='Comma separated list of fill(s) to reprocess.')
    inject_parser.add_argument('--era',
                               dest='era',
                               default=None,
                               required=True,
                               type=str,
                               help='Inject all runs belonging to a given CMS data acquisition era.')
    inject_parser.add_argument('--globaltag',
                               dest='globaltag',
                               default=None,
                               required=True,
                               type=str,
                               help='Specify CMS GlobalTag for the entire reprocessing.')
    inject_parser.add_argument('--certjson',
                               dest='certjson',
                               default=None,
                               type=str,
                               help='JSON file with certified runs.')

    # inject_mc subcommand
    # options to setup a new reprocessing campaign
    inject_mc_parser = subparsers.add_parser('inject-mc', help='Inject new MC in a campaign')
    inject_mc_parser.set_defaults(func=inject_campaign_mc)
    add_campaign_opt(inject_mc_parser)
    inject_mc_rlist = inject_mc_parser.add_mutually_exclusive_group(required=False)
    inject_mc_rlist.add_argument('--runs',
                                 dest='runs',
                                 default='',
                                 type=lambda opt: opt.split(',') if opt else None,
                                 help='Comma separated list of run(s) to inject in the new campaign.')
    inject_mc_rlist.add_argument('--range',
                                 dest='rrange',
                                 default='',
                                 type=lambda opt: opt.split(',') if opt else [None, None],
                                 help='Define a range of runs to be injected (min,max).')
    inject_mc_parser.add_argument('--era',
                                  dest='era',
                                  default=None,
                                  required=True,
                                  type=str,
                                  help='Specify era name.')
    inject_mc_parser.add_argument('--globaltag',
                                  dest='globaltag',
                                  default=None,
                                  required=True,
                                  type=str,
                                  help='Specify CMS GlobalTag for the entire reprocessing.')

    # sync subcommand
    sync_parser = subparsers.add_parser('sync', help='Inject runs acquired since last inserted run')
    sync_parser.set_defaults(func=sync_runs)
    add_campaign_opt(sync_parser)
    mode_opt = sync_parser.add_mutually_exclusive_group(required=False)
    mode_opt.add_argument('--all',
                          dest='all',
                          default=True,
                          action='store_true',
                          help='Fetch all new runs')
    mode_opt.add_argument('--single',
                          dest='all',
                          default=True,
                          action='store_false',
                          help='Fetch only one new run (mainly for debug)')
    gt_opt = sync_parser.add_mutually_exclusive_group(required=False)
    gt_opt.add_argument('--prompt',
                        dest='globaltag',
                        default='prompt',
                        const='prompt',
                        action='store_const',
                        help='Inject runs using the Prompt GT (from Tier0)')
    gt_opt.add_argument('--express',
                        dest='globaltag',
                        const='express',
                        action='store_const',
                        help='Inject runs using the Express GT (from Tier0)')
    gt_opt.add_argument('--hlt',
                        dest='globaltag',
                        const='hlt',
                        action='store_const',
                        help='Inject runs using the HLT GT (from OMS)')

    # status subcommand
    status_parser = subparsers.add_parser('status', help='Get status for specified run(s)')    
    status_parser.set_defaults(func=status)
    add_campaign_opt(status_parser)    
    status_parser.add_argument('--runs',
                               dest='runs',
                               type=str,
                               default=None,
                               nargs='+',
                               help='check status of space-separated list of runs')
    status_parser.add_argument('--reply',
                               dest='reply',
                               default=None,
                               type=str,
                               help='Mattermost slash command response URL')

    # list campaigns
    list_campaigns_parser = subparsers.add_parser('list-campaigns', help='List campaigns available in the database')
    list_campaigns_parser.set_defaults(func=list_campaigns)
    list_campaigns_parser.add_argument('-m', '--minimal',
                                       dest='minimal',
                                       default=False,
                                       required=False,
                                       action='store_true',
                                       help='Print only the available campaigns names to stdout using print instead of logging')
    
    # close subcommand
    close_parser = subparsers.add_parser('close', help='Check for completed task and mark runs as fully processed in the influxdb')
    close_parser.set_defaults(func=close_processing)
    close_parser.add_argument('-c', '--campaign',
                              dest='campaign',
                              default='prompt',
                              required=True,
                              nargs='+',
                              type=str,
                              help='Processing campaign(s). "all" for all campaigns in the db')
    
    # create run type subcommand
    rtype_creates_parser = subparsers.add_parser('rtype-create', help='Insert a new run type with its workflows list')
    rtype_creates_parser.set_defaults(func=create_rtype)
    add_campaign_opt(rtype_creates_parser)
    rtype_creates_parser.add_argument('--type',
                                      dest='rtype',
                                      required=True,
                                      type=str,
                                      help='New run type name')
    rtype_creates_parser.add_argument('--rr-filter',
                                      dest='rr_filter',
                                      required=True,
                                      type=str,
                                      help='New run type OMS query filter')
    rtype_creates_parser.add_argument('--copy',
                                      dest='from_type',
                                      default=None,
                                      type=str,
                                      help='Run type from which to copy the list of workflows')
    rtype_creates_parser.add_argument('--add',
                                      dest='wflows_add',
                                      default='',
                                      type=str,
                                      help='workflows to add (comma separated list)')

    # update subcommand
    update_wflows_parser = subparsers.add_parser('rtype-update', help='Update the list of active/inactive workflows')
    update_wflows_parser.set_defaults(func=update_wflows)
    add_campaign_opt(update_wflows_parser)
    update_wflows_parser.add_argument('--add',
                                      dest='wflows_add',
                                      default='',
                                      type=str,
                                      help='workflows to add (comma separated list)')
    update_wflows_parser.add_argument('--remove',
                                      dest='wflows_remove',
                                      default='',
                                      type=str,
                                      help='workflows to remove (comma separated list)')
    update_wflows_parser.add_argument('--rr-filter',
                                      dest='rr_filter',
                                      default=None,
                                      type=str,
                                      help='Update OMS query filter')
    update_wflows_parser.add_argument('--type',
                                      dest='rtype',
                                      required=True,
                                      type=str,
                                      help='Run type')

    # list subcommand
    list_wflows_parser = subparsers.add_parser('rtype-list', help='List the list of active/inactive workflows')
    list_wflows_parser.set_defaults(func=list_wflows)
    add_campaign_opt(list_wflows_parser)
    list_wflows_parser.add_argument('--type',
                                    dest='type',
                                    default=None,
                                    type=str,
                                    help='Run type')

    return parser


if __name__ == '__main__':
    opts = cmdline_options().parse_args()

    stream_handler = logging.StreamHandler()
    stream_handler.setLevel(logging.INFO)
    stream_handler.setFormatter(logging.Formatter('[RUN-CTRL]: %(message)s'))
    logging.basicConfig(level=logging.INFO)
    log = logging.getLogger()
    # remove default logger
    log.handlers.pop(0)
    # console logger
    log.addHandler(stream_handler)
    if opts.notify:
        # Mattermost logger if configured through cmd line option.
        mm_handler = MattermostHandler(url=opts.notify)
        mm_handler.setFormatter(logging.Formatter('[RUN-CTRL]: %(message)s'))
        mm_handler.setLevel(logging.WARNING)
        log.addHandler(mm_handler)

    ret = opts.func(**vars(opts))

    sys.exit(ret)

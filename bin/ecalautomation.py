#!/usr/bin/env python3

import os
import sys
import time
import argparse

import ecalautoctrl as ectrl

class SplitArgs(argparse.Action):
    def __call__(self, parser, namespace, values, option_string=None):
        setattr(namespace, self.dest, values.split(','))

class SplitDict(argparse.Action):
    def __call__(self, parser, namespace, values, option_string=None):
        setattr(namespace, self.dest, {t.split(':',1)[0] : t.split(':',1)[1] for t in values.split(',')})
        
def exist(opts):
    """
    Check if task exist in the db
    """

    jctrl = ectrl.JobCtrl(task=opts.task, campaign=opts.campaign, tags=opts.tags, dbname=opts.dbname) 
    exist = jctrl.taskExist()
    
    print('Task %s+%s+%s %s' % (opts.task, opts.campaign, opts.tags, 'exist' if exist else 'does not exist'))
    
    return exist

def submit(opts):
    """
    Inject a new task into the DB
    """

    jctrl = ectrl.JobCtrl(task=opts.task, campaign=opts.campaign, tags=opts.tags, dbname=opts.dbname) 
    if opts.ids:
        jctrl.createTask(jids=[id.strip() for id in opts.ids.split(',')])
    elif opts.njobs:
        jctrl.createTask(jids=[id for id in range(opts.njobs)])
    else:
        sys.exit('[ecalsubmit] Please specify one option between --ids and -n')    

    return 0

def jobctrl(opts):
    """
    Update job status in the DB
    """
    
    jctrl = ectrl.JobCtrl(task=opts.task, campaign=opts.campaign, tags=opts.tags, dbname=opts.dbname)
    if not opts.fields:
        opts.fields = []
    opts.fields = {ff.split(':',1)[0] : ff.split(':',1)[1] for ff in opts.fields}
    if opts.idle:
        jctrl.idle(opts.id, fields=opts.fields)
    if opts.running:
        jctrl.running(opts.id, fields=opts.fields)
    if opts.failed:
        jctrl.failed(opts.id, fields=opts.fields)
    if opts.done:
        jctrl.done(opts.id, fields=opts.fields)

    return 0

def status(opts):
    """
    Get current jobs status
    """
    
    jctrl = ectrl.JobCtrl(task=opts.task, campaign=opts.campaign, tags=opts.tags, dbname=opts.dbname)

    jobs = None
    if opts.idle:
        print('Idle jobs for task %s + %s + %s' % (opts.task, opts.campaign, opts.tags))
        print(jctrl.getIdle())

    if opts.running:
        print('Running jobs for task %s + %s + %s' % (opts.task, opts.campaign, opts.tags))
        print(jctrl.getRunning())

    if opts.failed:
        print('Failed jobs for task %s + %s + %s' % (opts.task, opts.campaign, opts.tags))
        print(jctrl.getFailed())

    if opts.done:
        print('Done jobs for task %s + %s + %s' % (opts.task, opts.campaign, opts.tags))
        print(jctrl.getDone())

    if opts.all:
        jobs = jctrl.getJobs()
        header = 'Jobs status for task: %s + %s + %s' % (opts.task, opts.campaign, opts.tags)
        print(header)
        print('='*len(header))
        total = 0
        for status, j in jobs.items():
            text = ' %s (%d):' % (status+' '*(15-len(status)-len(str(len(j)))), len(j))
            print(text, end=' ')
            print(j)
            total += len(j)
        print(' total %s : %d' % (' '*(13-len(str(total))), total))

    if opts.id:
        print('Info for job %s in task %s + %s + %s' % (str(opts.id), opts.task, opts.campaign, opts.tags))
        print(jctrl.getJob(opts.id))

def wait(opts):
    """
    Wait until all jobs are in status done
    """

    while True:
        jctrl = ectrl.JobCtrl(task=opts.task, campaign=opts.campaign, tags=opts.tags, dbname=opts.dbname)
        jobs = jctrl.getJobs()
        prev_failed = set([])
        
        total = len(jobs['idle']+jobs['running']+jobs['failed']+jobs['done'])
        if len(jobs['done']) == total:
            print('All jobs in task %s + %s + %s successfully completed' % (opts.task, opts.campaign, opts.tags))
            return
        elif len(jobs['failed']) > 0 and set(jobs['failed']) != set(prev_failed):
            if opts.resubcmd != '':
                os.system(opts.resubcmd)
            elif opts.jobresubcmd != '':
                for jid in jobs['failed']:
                    os.system('export JOBID='+str(jid)+'; '+opts.jobresubcmd)
            prev_failed = set(jobs['failed'])
            
        time.sleep(opts.sleep)

### dictionary of available subcommands
subcommands = {
    'exist'     : exist,
    'submit'    : submit,
    'jobctrl'   : jobctrl,
    'status'    : status,
    'wait'      : wait,
}

def cmdline_options():
    """
    Return the argparse instance to handle the cmdline options for this script (needed to easily generate 
    the docs using sphinx-argparse)
    """
    
    ### global options
    parser = argparse.ArgumentParser(description=
    """
    Update task/job information to the ECAL automation influxdb.

    + Example submit (called by submit script):
    ecalautomation.py -c new_campaign -w ECALELF_prod -t era:2021A submit -n 100

    + Example job start running (called by job script):     
    ecalautomation.py -c new_campaign -w ECALELF_prod -t era:2021A jobctrl --id 4 --running
    """, formatter_class=argparse.RawTextHelpFormatter)
    
    parser.add_argument('-c', '--campaign', dest='campaign',
                        help='ECAL processing campaign', required=True)
    parser.add_argument('-w', '--task', dest='task',
                        help='ECAL task', required=True)
    parser.add_argument('-t', '--tags', dest='tags', default='',
                        action=SplitDict, help='User defined tags')
    parser.add_argument('--db', dest='dbname', default='ecal_offline_test',
                        type=str, help='Specify to which db write the data')

    ### sub commands
    subparsers = parser.add_subparsers(dest='subcommand', description='Select the DB operation')

    ### exist subcommand
    exist_parser = subparsers.add_parser('exist', help='Check if task already exist')
    
    ### submit subcommand
    submit_parser = subparsers.add_parser('submit', help='Inject a submitted task into the DB')
    submit_parser.add_argument('--ids', dest='ids', default=None, type=str, 
                               help='List of submitted jobs, if this option is specified option -N is ignored')    
    submit_parser.add_argument('-n', dest='njobs', default=None, type=int,
                               help='Number of submitted jobs, the script will assume that jobs ids span from 0 to N-1') 

    ### job control subcommand
    jobctrl_parser = subparsers.add_parser('jobctrl', help='Update job status in the DB')
    group = jobctrl_parser.add_mutually_exclusive_group(required=True )
    group.add_argument('--idle', action='store_true')
    group.add_argument('--running', action='store_true')
    group.add_argument('--failed', action='store_true')
    group.add_argument('--done', action='store_true')
    jobctrl_parser.add_argument('--id', dest='id', help='Job id within the task', required=True)
    jobctrl_parser.add_argument('--fields', dest='fields', type=str, nargs='+', default=None,
                                help='Extra job fields')

    ### status subcommand
    status_parser = subparsers.add_parser('status', help='Jobs status informations from the DB')
    group = status_parser.add_mutually_exclusive_group(required=True )
    group.add_argument('--idle', action='store_true')
    group.add_argument('--running', action='store_true')
    group.add_argument('--failed', action='store_true')
    group.add_argument('--done', action='store_true')
    group.add_argument('--all', action='store_true')
    group.add_argument('--id', dest='id', default=None, help='Job id within the task')    

    ### wait subcommand
    wait_parser = subparsers.add_parser('wait', help='Wait for all jobs in task to be done')
    wait_parser.add_argument('-s', dest='sleep', type=int, default=300, help='Period between checks in seconds')    
    wait_parser.add_argument('--resubcmd', dest='resubcmd', type=str, default='', help='Resubmit command (executed once for all failed jobs)')    
    wait_parser.add_argument('--jobresubcmd', dest='jobresubcmd', type=str, default='', help='Resubmit command (executed once for each failed job). The jobid is available in the $JOBID environment variable. Example: `my_resubmit.sh $JOBID`')    

    # ### listdsets subcommand
    # listdsets_parser = subparsers.add_parser('listdsets', help='Get all available datasets')
    
    ### getfiles subcommand
    getfiles_parser = subparsers.add_parser('getfiles', help='Get files for the specified task')
    group = getfiles_parser.add_mutually_exclusive_group(required=True )
    group.add_argument('--all', action='store_true')
    group.add_argument('--id-range', dest='id_range', action=SplitArgs, help='List containing [first_id, n_files]')
    getfiles_parser.add_argument('--type', dest='type', default=None, help='File type')    
    getfiles_parser.add_argument('--data', dest='data', action=SplitArgs, default=['path'], help='File data to be retrived')    

    ### setfile subcommand
    setfile_parser = subparsers.add_parser('setfile', help='Insert/update file info in the influxdb')
    setfile_parser.add_argument('--path', dest='path', default=None, help='File path', required=True)    
    setfile_parser.add_argument('--id', dest='id', default=None, help='Job id within the task', required=True)   
    setfile_parser.add_argument('--type', dest='type', default=None, help='File type')    
    setfile_parser.add_argument('--runs-lumis', dest='runs_lumis', default=None, help='JSON string containing the CMSSW like list of runs and lumis contained in the file')    
    setfile_parser.add_argument('--fills', dest='fills', default=None, action=SplitArgs, help='Comma separated list of fills contained in the file')    
    setfile_parser.add_argument('--valid', dest='valid', default=None, type=bool, help='File integrity has been valided')    
    return parser

if __name__ == '__main__':
    """
    This script is designed to provide a standard interface to the ECAL automation influxdb.
    """    
    
    opts = cmdline_options().parse_args()

    ret = subcommands[opts.subcommand](opts)

    sys.exit(ret)

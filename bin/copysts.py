#!/usr/bin/env python3

from ecalautoctrl.credentials import dbhost, dbport, dbusr, dbpwd, dbssl

import argparse
import logging
import sys
from influxdb import InfluxDBClient

def cmdline_options():
    """
    Return the argparse instance to handle the cmdline options for this script.

    The function is needed by sphinx-argparse to easily generate the docs.
    """
    parser = argparse.ArgumentParser(description="""
    Duplicate data from one db to another. Useful for running tests using output produced within existing campaigns. Note that due to limitations in influxdb the campaign name cannot be changed.
    The target database must be created by an admin user before running this script.
    """, formatter_class=argparse.RawTextHelpFormatter)

    parser.add_argument('--input-db',
                        dest='input_db',
                        required=True,
                        type=str,
                        help='Input database')
    parser.add_argument('--output-db',
                        dest='output_db',
                        required=True,
                        type=str,
                        help='Output database (must already exist)')
    parser.add_argument('-c', '--campaign',
                        dest='campaign',
                        default='prompt',
                        type=str,
                        help='Input campaign name, output will be the same')
    filters = parser.add_mutually_exclusive_group(required=False)
    filters.add_argument('--runs',
                         dest='runs',
                         default='',
                         type=lambda opt: opt.split(',') if opt else None,
                         help='Comma separated list of run(s) to inject in the new campaign.')
    filters.add_argument('--range',
                         dest='rrange',
                         default='',
                         type=lambda opt: opt.split(',') if opt else [None, None],
                         help='Define a range of runs to be injected (min,max).')
    filters.add_argument('--fills',
                         dest='fills',
                         default='',
                         type=lambda opt: opt.split(',') if opt else None,
                         help='Comma separated list of fill(s) to copy.')
    filters.add_argument('--all',
                         dest='all',
                         default=False,
                         action='store_true',
                         help='Copy all runs')
    
    return parser

if __name__ == '__main__':
    opts = cmdline_options().parse_args()

    dbclt = InfluxDBClient(host=dbhost,
                           port=dbport,
                           username=dbusr,
                           password=dbpwd,
                           ssl=dbssl,
                           database=opts.input_db,
                           timeout=30_000)

    # construct filter
    if opts.all:
        copyf = f'WHERE "campaign"=\'{opts.campaign}\' '
    elif opts.fills:
        copyf = f'WHERE "campaign"=\'{opts.campaign}\' AND ('
        copyf += ' OR '.join([f'"fill"=\'{f}\'' for f in opts.fills])
        copyf += ' )'
    elif opts.runs:        
        copyf = f'WHERE "campaign"=\'{opts.campaign}\' AND ('
        copyf += ' OR '.join([f'"run_number"=\'{r}\'' for r in opts.runs])
        copyf += ' )'
    elif opts.range:
        copyf = f'WHERE "campaign"=\'{opts.campaign}\' AND "run_number">=\'{opts.range[0]}\' AND "run_number"<+\'{opts.range[1]}\''

    # copy query
    for measurement in ['run', 'job']:
        query = f'SELECT * INTO {opts.output_db}..{measurement} FROM {opts.input_db}..{measurement} {copyf} GROUP BY *' 
        dbclt.query(query)
    
    

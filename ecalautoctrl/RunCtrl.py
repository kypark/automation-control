from ecalautoctrl.credentials import dbhost, dbport, dbusr, dbpwd, dbssl
from ecalautoctrl.influxdb_utils import reformat_last
from omsapi import OMSAPI, OMS_FILTER_OPERATORS
# add missing operator to OMS (check in the future)
OMS_FILTER_OPERATORS.append('CT')

from typing import List, Dict, Optional, Any, Set, Tuple
from datetime import datetime
from influxdb import InfluxDBClient
import os
import sys
import logging

class RunCtrl:
    """
    Update ECAL influxdb instance with new run info.

    The run status is represented by the following table::

       {
           'measurement' : 'job',
           'tags' : {
               'campaign'   : 'Campaign to which the run belongs to',
               'run_number' : 'CMS run number',
               'fill'       : 'LHC fill'               
           },
           'time' : timestamp,
           'fields' : {
               'endtime' : 'string - formatted timestamp DAQ end run)',
               'delay_h' : 'int - delay between last update and DAQ end time',
               'era' : 'string - CMS processing era',
               'globaltag' : 'string - processing GT',
               'certjson' : 'string - path to the certification json used to inject the campaign',
               'lumi' : 'int - integrated luminosity for this run in /pb',
               'status_change_link' : 'link to jenkins build | manual',
               'global' : 'new|processing|failed|done',
               'task' : 'new|reprocess|processing|failed|done|skipped'
           }
       }

    task is a field repeated for each task enabled in the campaign.
    
    :param dbname: database instance name (default is test db)
    :param campaign: campaign name (default is test)
    """
    
    def __init__(self,
                 dbname: str,
                 campaign: str) -> None:
        self.db = InfluxDBClient(host=dbhost,
                                 port=dbport,
                                 username=dbusr,
                                 password=dbpwd,
                                 ssl=dbssl,
                                 database=dbname,
                                 timeout=30_000)
        self.campaign = campaign

        self.oms = OMSAPI("https://cmsoms.cern.ch/agg/api", "v1", cert_verify=False, verbose=False)
        self.oms.auth_oidc('omsapi-espresso', '8f41d691-e003-4552-b446-c42f4e0882fb')

    def injectRun(self,
                  run,
                  acq_era: str,
                  gt: str,
                  fill: int,
                  status: Dict[str, str],
                  certjson: str = None,
                  lumi: float = None,
                  run_endtime: str = None):
        """
        Inject a new run into the db.

        :param run: CMS run number.
        :param acq_era: T0 acquisition era.
        :param gt: reconstruction global tag.
        :param fill: LHC fill number.
        :param status: tasks status.
        :param certjson: location of the certification json to be used by the jobs.
        :param lumi: integrated luminosity.
        :param run_endtime: UTC run stop time.
        """
        fields = dict(status)
        fields['global'] = 'processing'
        fields['era'] = acq_era
        fields['globaltag'] = gt
        fields['certjson'] = certjson
        fields['lumi'] = float(lumi) if lumi else 0.
        fields['status_change_link'] = os.environ['BUILD_URL'] if 'BUILD_URL' in os.environ else 'manual'
        if run_endtime:
            # delay from end run in hours
            delay = (datetime.utcnow() - datetime.strptime(run_endtime, '%Y-%m-%dT%H:%M:%SZ')).total_seconds()/60./60.
            fields['endtime'] = run_endtime
            fields['delay_h'] = delay        

        return self.db.write_points([{
            'measurement' : 'run',
            'tags' : { 'run_number' : run, 'fill' : fill, 'campaign' : self.campaign },
            'fields' : fields,
            'time' : datetime.utcnow().strftime("%Y-%m-%dT%H:%M:%SZ")
        }])
    
    def updateStatus(self,
                     run: int,
                     status: Dict[str, str]=None) -> bool:
        """
        Update a single run status in the influxdb.
        The status for different tasks can be updated independently.

        :param run: run to be updated.
        :param status: status map.
        """

        # do not update if no status change
        if not status or len(status)==0:
            return False

        query = self.db.query(f'SELECT * FROM "run" WHERE "run_number" = \'{run}\' AND "campaign" = \'{self.campaign}\' ORDER BY time DESC')
        
        if not query:
            sys.exit(
                f'[RunCtrl::updateStatus] Run {run} not yet injected for campaign {self.campaign}.'
            )

        q = self.oms.query('runs')
        q.sort('run_number', asc=True)
        q.paginate(page=1, per_page=10000)
        q.filter('run_number', int(run))
        q.attrs(['fill_number','recorded_lumi'])
        
        last = next(query.get_points())
        prev_data = {k : v for k, v in last.items() if v}
        # retrieve missing information (if any) was not correctly set
        if 'fill' not in prev_data.keys():
            prev_data['fill'] = q.data().json()['data'][-1]['attributes']["fill_number"]
        if 'lumi' not in prev_data.keys():
            prev_data['lumi'] = q.data().json()['data'][-1]['attributes']['recorded_lumi']
        prev_data['lumi'] = prev_data['lumi'] if prev_data['lumi'] else 0.
        fill = prev_data['fill']

        # compute delay and push update to the influxdb
        delay_h=0
        if 'endtime' in prev_data.keys():
            delay_h = (datetime.utcnow() - datetime.strptime(prev_data['endtime'], '%Y-%m-%dT%H:%M:%SZ')).total_seconds()/60./60.
        # generate new status
        status.update({'delay_h' : delay_h,
                       'status_change_link' : os.environ['BUILD_URL'] if 'BUILD_URL' in os.environ else 'manual',
                       'endtime' : prev_data['endtime'],
                       'lumi' : prev_data['lumi']})
        ret = self.db.write_points([{
                'measurement' : 'run',
                'tags' : { 'run_number' : run, 'fill' : fill, 'campaign' : self.campaign },
                'fields' : status,
                'time' : datetime.utcnow().strftime("%Y-%m-%dT%H:%M:%SZ")
        }])

        return ret
   
    def getWorkflows(self, tag: str = None) -> Dict:
        """
        Return the list of workflows that are part of the automation.

        :param tag: workflows type.
        """
        if tag:
            data = self.db.query(f'SELECT last(*) FROM "workflows" WHERE "campaign"=\'{self.campaign}\' AND "type"=\'{tag}\' GROUP BY "type"')
        else:
            data = self.db.query(f'SELECT last(*) FROM "workflows" WHERE "campaign"=\'{self.campaign}\' GROUP BY "type"')

        workflows = {wf['type']: {} for i, wf in data.keys()}
        for wf in data.keys():
            info = next(data[wf])
            workflows[wf[-1]['type']]['rr_filter'] = info['last_rr_filter']
            workflows[wf[-1]['type']]['active'] = set(info['last_active'].split(',')) if 'last_active' in info and info['last_active'] else set([])
            workflows[wf[-1]['type']]['inactive'] = set(info['last_inactive'].split(',')) if 'last_inactive' in info and info['last_inactive'] else set([])
            # sanitize
            workflows[wf[-1]['type']]['active'].discard('')
            workflows[wf[-1]['type']]['inactive'].discard('')

        return workflows

    def createRunType(self, tag: str,
                      rr_filter: str,
                      add: Optional[str] = None,
                      from_tag: Optional[str] = None):
        """
        Insert a new run type in the db.

        :param tag: run type name.
        :param rr_filter: filter for the OMS query.
        :param add: list of workflows to add.
        :param from_tag: copy workflows from another run type.
        """
        if tag in self.getWorkflows():
            logging.warning('{tag} already exists')
            return 0

        data = {'measurement': 'workflows',
                'tags': {'type': tag, 'campaign': self.campaign},
                'time': datetime.utcnow().strftime("%Y-%m-%dT%H:%M:%SZ"),
                'fields': {'active': None,
                           'inactive': None,
                           'rr_filter': rr_filter}}

        if from_tag:
            ext = self.getWorkflows(tag=from_tag)[from_tag]
            data['fields']['active'] = ext['active']
            data['fields']['inactive'] = ext['inactive']

        self.db.write_points([data])

        if add:
            self.updateWorkflows(tag=tag, add=add)
    
    def updateWorkflows(self,
                        tag: str = None,
                        add: List[str] = None,
                        remove: List[str] = None,
                        rr_filter: str = None):
        """
        Update the list of active workflows for a given type of runs.

        :param tag: run type.
        :param add: list of workflows to add.
        :param remove: list of workflows to remove.
        :param rr_filter: update OMS query filter.
        """
        if not tag:
            sys.exit('[RunCtrl::updateWorkflows] Please specify a tag.')

        current = self.getWorkflows(tag=tag)
        if tag in current:
            if remove:
                for task in remove:
                    if task in current[tag]['active']:
                        current[tag]['active'].discard(task)
                    if task not in current[tag]['inactive']:
                        current[tag]['inactive'].update([task])
            if add:
                for task in add:
                    if task in current[tag]['inactive']:
                        current[tag]['inactive'].discard(task)
                    if task not in current[tag]['active']:
                        current[tag]['active'].update([task])
            if rr_filter:
                current[tag]['rr_filter'] = rr_filter
        else:
            sys.exit(
                f'[RunCtrl::updateWorkflows] Tag {tag} does not exist.'
            )

        return self.db.write_points([{'measurement': 'workflows',
                                      'tags': {'type': tag,
                                               'campaign': self.campaign},
                                      'time': datetime.utcnow().strftime("%Y-%m-%dT%H:%M:%SZ"),
                                      'fields': {'active': ','.join(current[tag]['active']),
                                                 'inactive': ','.join(current[tag]['inactive']),
                                                 'rr_filter': current[tag]['rr_filter']}
                                      }])

    def getRuns(self,
                status: Optional[Dict[str, str]] = None,
                active: Optional[bool] = True) -> List[Dict]:
        """
        Retrieve runs from the ECAL db satisfying certain conditions.

        :param status: list of statuses the run has reached (if more than one AND is assumed).
        :param active: runs that are in "global"='processing' status.
        """
        selection = '"last_global"=\'processing\'' if active else '"last_global"!=\'processing\''
        # filter run by status
        if status:
            sts_filter = ' AND '.join([f'"last_{s}" = \'{v}\'' for s,v in status.items()])
            selection += ' AND ' + sts_filter
        data = self.db.query(f'SELECT *,"run_number" FROM (SELECT last(*) FROM "run" WHERE "campaign"=\'{self.campaign}\' GROUP BY run_number,fill) WHERE {selection} GROUP BY "run_number"')

        return reformat_last(data)

    def getStatus(self, run: int) -> Dict:
        """
        Get run last status.

        :param run: CMS run number.
        :return: run status dictionary.
        """
        data = self.db.query(f'SELECT last(*) FROM "run" WHERE "campaign"=\'{self.campaign}\' AND "run_number"=\'{run}\'')

        return reformat_last(data)

    def getRunStatusChangeTime(self, run: int, status: Tuple[str, str]):
        """
        Get time at which the specified status change happened

        :param run: CMS run number.
        :param status: workflow status change.
        :rtype: datetime, None if run/campaign/status change not found.
        """
        data = reformat_last(self.db.query(f'SELECT last("delay_h") FROM "run" WHERE "campaign"=\'{self.campaign}\' AND "run_number"=\'{run}\' AND "{status[0]}"=\'{status[1]}\' ORDER BY time DESC')        )

        if not data:
            return None

        return datetime.strptime(data[0]['time'], "%Y-%m-%dT%H:%M:%SZ") 

    def getRunLastUpdate(self, run: int):
        """
        Retrive the timestamp of the last update for the given run/campaign.

        :param run: CMS run number.
        """
        data = self.db.query(f'SELECT last("global") FROM "run" WHERE "run_number"=\'{run}\' AND "campaign"=\'{self.campaign}\'')

        return reformat_last(data)[-1]['time']
    
    def getLastRun(self) -> int:
        """
        Retrieve last run. This is the most recent run acquired in CMS and injected
        in the automation workflow (i.e. the largest run number). This is not necessarly
        the last run injected chronologically into the system.
        """
        runs = reformat_last(self.db.query(f'SELECT * FROM (SELECT last(*) FROM "run" WHERE "campaign"=\'{self.campaign}\' GROUP BY run_number)'))

        return int(max(runs, key=lambda r : r['run_number'])['run_number'])

    def getCertificationJSON(self, run: int) -> str:
        """
        Returns the path to the certification JSON specified during injection.

        Certification files are provided only for reprocessing.

        :param run: The CMS run number.
        :return: Path to the certification JSON.
        """

        path = reformat_last(self.db.query(f'SELECT last(certjson) FROM "run" WHERE "campaign"=\'{self.campaign}\' AND "run_number"=\'{run}\')'))[-1]['certjson']
        
        return path
        
    def getField(self,
                 process: str,
                 field: str = 'output',
                 runs: Optional[List[Any]] = None,
                 fills: Optional[List[Any]] = None,
                 era: Optional[str] = None) -> Set:
        """
        Retrieve the list of fields for a given process and list of runs/fills/era.
        `runs` takes precedence over `fills` which in turn takes precedence over `era`.

        :param process: processing step.
        :param field: field name.
        :param runs: list of CMS runs.
        :param fills: list of LHC fills.
        :param era: T0 acquisition era.
        :return: the list of field values.
        """

        # if runs is not set collect wanted runs based on the other two criteria
        if not runs and not fills and not era:
            selection = f'"task"=\'{process}\' AND "campaign"=\'{self.campaign}\' AND done = 1'
            data = self.db.query(f'SELECT {field} FROM "job" WHERE {selection}')
            # remove possible duplicates
            return set([d[field] for d in data.get_points()])
        else:
            if not runs and not fills:
                runs = self.getRunsInEra(era=era, task=process)
            if not runs and fills:
                runs = []
                for fill in fills:
                    runs.extend(self.getRunsInFill(fill=int(fill), task=process))
            # prevent query string from getting too long by batching runs
            sbatch = 0
            batch_size = 30 # arbitrary
            data = []
            while sbatch < len(runs):
                run_sel = ' OR '.join([f'"run_number"=\'{run}\'' for run in runs[sbatch:sbatch+batch_size]])
                sbatch += batch_size
                selection = f'({run_sel}) AND "task"=\'{process}\' AND "campaign"=\'{self.campaign}\' AND done = 1'            
                data.extend([d[field] for d in self.db.query(f'SELECT {field} FROM "job" WHERE {selection}').get_points()])
            # remove possible duplicates
            return set(data)


    def getOutput(self, process: str,
                  runs: Optional[List[Any]]=None,
                  fills: Optional[List[Any]]=None,
                  era: Optional[str]=None) -> Set:
        """
        Retrieve the list of output files for a given process and list of runs/fills/era.
        `runs` takes precedence over `fills` which in turn takes precedence over `era`.

        :param process: processing step.
        :param runs: list of CMS runs.
        :param fills: list of LHC fills.
        :param era: T0 acquisition era.
        :return: list of outputs.
        """

        return self.getField(process=process, field='output', runs=runs, fills=fills, era=era)
        
    def getRunsInFill(self, fill: int, task: str=None) -> List:
        """
        Get all runs belonging to a fill

        :param fill: LHC fill number.
        :param task: if set collects only runs for which the task was injected.
        """

        selection = f'"campaign"=\'{self.campaign}\' AND "fill"=\'{fill}\''
        if task:
            selection += f' AND "{task}" != \'\''
        data = self.db.query(f'SELECT last(*) FROM "run" WHERE {selection} GROUP BY "run_number"')  

        return [k[-1]['run_number'] for k,v in data.items()]

    def getRunsInEra(self, era: str, task: str=None) -> List:
        """
        Get all runs belonging to a fill

        :param era: T0 acquisition era.
        :param task: if set collects only runs for which the task was injected.
        """

        selection = f'"campaign"=\'{self.campaign}\' AND "era"=\'{era}\''
        if task:
            selection += f' AND "{task}" != \'\''
        data = self.db.query(f'SELECT last(*) FROM "run" WHERE {selection} GROUP BY "run_number"')  

        return [k[-1]['run_number'] for k,v in data.items()]
    
    def fillDumped(self, fill: int) -> bool:
        """
        Check if fill has been completed.

        This is done by checking that the end_time field is filled in OMS.

        :param fill: LHC fill number.
        """
        q = self.oms.query('fills')
        q.filter('fill_number', fill, 'EQ')
        return q.data().json()['data'][-1]['attributes']['end_time'] is not None

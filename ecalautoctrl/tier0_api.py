'''
This module provide an general interface to the T0 API.
'''

import requests
import logging
from typing import Dict

class T0GeneralAPI():
    """
    This class provides access to the 'standard' T0 API which is accessible
    via HTTPS queries.
    """

    def __init__(self):
        self.t0_url = f'https://cmsweb.cern.ch/t0wmadatasvc/prod/'

    def __str__(self):
        return 'T0GeneralAPI'

    def get_MRH_values(self, run: int) -> Dict[str,str]:
        """
        Returns a dictionary of values of the express processing for the specified run.

        :param run:  CMS run number.
        """
        # Return object
        MRH_values = {}

        # Compose URL
        target_url = self.t0_url + 'express_config?run=' + str(run)

        # Query
        resp = requests.get(url=target_url, verify=False).json()['result']

        # Parse
        if len(resp):
            for dset in resp:
                MRH_values['cmssw']      = dset['cmssw']
                MRH_values['scram_arch'] = dset['scram_arch']
                MRH_values['global_tag'] = dset['global_tag']
        else:
            # if not available for the current run get the value
            # from the current run under processing in T0
            resp = requests.get(url=self.t0_url + '/express_config', verify=False).json()['result']
            if len(resp):
                for dset in resp:
                    MRH_values['cmssw']      = dset['cmssw']
                    MRH_values['scram_arch'] = dset['scram_arch']
                    MRH_values['global_tag'] = dset['global_tag']

        return MRH_values

    def get_gt(self, run: int, rtype: str='prompt') -> str:
        """
        Return the Prompt/Express GT as string for the specified run.

        :param run:  CMS run number.
        :param rtype: reco type: prompt or express.
        """
        if rtype not in ['prompt', 'express']:
            logging.error(f'Specified reco ({rtype}) type not supported. Please choose prompt or express')
            sys.exit(1)

        config_name = 'reco_config' if rtype=='prompt' else 'express_config'
            
        # Return value
        t0gt = ''

        # Compose URL
        target_url = self.t0_url + config_name + '?run=' + str(run)

        # Query
        resp = requests.get(url=target_url, verify=False).json()['result']

        # Parse
        if len(resp):
            for dset in resp:
                if 'ppEra' in dset['scenario']:
                    t0gt = dset['global_tag']
        else:
            # if global_tag not available for the current run get the value
            # from the current run under processing in T0
            resp = requests.get(url=f'{self.t0_url}/{config_name}', verify=False).json()['result']
            t0gt = resp[0]['global_tag']

        return t0gt

    
    def get_prompt_gt(self, run: int) -> str:
        """
        Return the Prompt GT as string for the specified run.

        :param run:  CMS run number.
        """
        return self.get_gt(run=run, rtype='prompt')

from ecalautoctrl.credentials import dbhost, dbport, dbusr, dbpwd, dbssl
from influxdb import InfluxDBClient
import logging
import json
import requests
from datetime import datetime
from pprint import pformat
from typing import List, Optional, Union
from .RunCtrl import RunCtrl
from .CMSTools import QueryOMS
from .tier0_api import T0GeneralAPI

def close_processing(dbname: str = 'test',
                     campaign: Union[str, List[str]] = 'prompt',
                     **kwargs):
    """
    Check for completed tasks and update the run status.

    :param dbname: database name.
    :param campaign: processing campaign, "all" for all active campaigns in the db.
    """
    # get all campaings if necessary
    if campaign == 'all':
        campaign = list_campaigns(dbname, quite=True)
    
    # multiple campaigns specified
    if isinstance(campaign, list):
        for c in campaign:
            close_processing(dbname, c)
    else:            
        rctrl = RunCtrl(dbname=dbname, campaign=campaign)
        runs = rctrl.getRuns(active=True)

        for run_dict in runs:
            run = run_dict['run_number']
            # get all the workflows
            tasks = len([k for k,s in run_dict.items() if s in ['new', 'reprocess', 'processing', 'done', 'failed'] and k!='global'])
            done = len([k for k,s in run_dict.items() if s=='done'])
            skipped = len([k for k,s in run_dict.items() if s=='skipped'])
            failed = len([k for k,s in run_dict.items() if s=='failed'])
            # all tasks ended succesfully
            if done+skipped == tasks:
                rctrl.updateStatus(run=run, status={'global' : 'done'})
                logging.info(f'Run {run} processing completed for campaign {campaign}.')
            # at least 1 failed task
            elif done+skipped+failed == tasks:
                rctrl.updateStatus(run=run, status={'global' : 'failed'})
                logging.error(f'One or more workflows failed for run {run} in campaign {campaign}')
            else:
                logging.info(f'Run {run} processing still ongoing for campaign {campaign}.')

    return 0


def reprocess_runs(dbname: str = 'test',
                   campaign: str = 'prompt',
                   tasks: List[str] = None,
                   era: str = None,
                   globaltag: str = None,
                   rrange: List[int] = None,
                   runs: List[int] = None,
                   fills: List[int] = None,
                   **kwargs):
    """
    Reprocess runs already injected.

    Works both with prompt and rereco databases.

    :param dbname: database name.
    :param campaign: processing campaign.
    :param tasks: list of workflows to reprocess.
    :param era: if specified overrides era from T0 API.
    :param globaltag: GT name to be used by all workflows.
    :param rrange: run range.
    :param runs: list of runs.
    :param fills: list of LHC fills.
    """
    rr = QueryOMS()
    rctrl = RunCtrl(dbname=dbname, campaign=campaign)

    n_runs = None
    if runs or fills:
        rrange = [None, None]
    else:
        rrange.sort()

    if len(rrange) != 2:
        logging.error(f'--range requires exactly 2 values (comma separated). {len(rrange)} provided.')

    # get workflows and runs
    # the same run can be selected with different filters
    sel_runs = {}
    for name, info in rctrl.getWorkflows().items():
        # skip class of runs when reprocessing
        if not any([t in info['active'] for t in tasks]):
            continue

        # get runs from OMS
        runs_data = rr.get_runs(filters=json.loads(info['rr_filter']),
                                runs=runs,
                                fills=fills,
                                min_run=rrange[0],
                                max_run=rrange[1],
                                daq_completed=True)
        for r in runs_data:
            # new run
            if r not in sel_runs:
                sel_runs[r] = runs_data[r]
                sel_runs[r]['workflows'] = {}

            sel_runs[r]['workflows'].update({wf: 'reprocess' for wf in info['active']})

    for r in sorted(sel_runs.keys())[:n_runs]:
        stss = rctrl.getStatus(run=r)
        # check if the run had been injected and has a status
        if len(stss) == 0:
            continue
        prev_sts = stss[-1]

        # copy GT and era unless overrided
        if era:
            era_inj = era
        else:
            era_inj = prev_sts['era']
        if globaltag:
            globaltag_inj = globaltag
        else:
            globaltag_inj = prev_sts['globaltag']

        # if injection is triggered by reprocessing set the correct status:
        if tasks and rctrl.getStatus(run=r):
            pop_list = set([])
            for sts in sel_runs[r]['workflows']:
                if sts not in tasks:
                    if sts in prev_sts.keys():
                        sel_runs[r]['workflows'][sts] = prev_sts[sts]
                    else:
                        pop_list.update([sts])
            for sts in pop_list:
                sel_runs[r]['workflows'].pop(sts)

        if rctrl.injectRun(r,
                           run_endtime=sel_runs[r]['end_time'],
                           acq_era=era_inj,
                           gt=globaltag_inj,
                           fill=sel_runs[r]['fill_number'],
                           status=sel_runs[r]['workflows'],
                           lumi=sel_runs[r]['recorded_lumi']):
            logging.info('Run %s marked for reprocessing in the %s db for workflows: %s', str(r), dbname, ','.join(sel_runs[r]['workflows'].keys()))
        else:
            logging.error('Run %s not injected as new in %s. Influxdb not reachable.', str(r), dbname)

    return 0


def sync_runs(dbname: str = 'test',
              campaign: str = 'prompt',
              globaltag: str = 'prompt',
              rall: bool = False,
              **kwargs):
    """
    Sync the automation db with the CMS DAQ by injecting runs in status new into the processing flow.

    This function is meant to be used for prompt data processing.
    For re-recos please use :func:`~create_campaign`.

    :param dbname: database name.
    :param campaign: processing campaign.
    :param globaltag: select GT for this campaign. Choice between "prompt", "express", "hlt".
    :param rall: sync all runs.
    """
    rr = QueryOMS()
    rctrl = RunCtrl(dbname=dbname, campaign=campaign)

    rrange = [rctrl.getLastRun()+1, None]
    n_runs = None if all else 1

    # get workflows and runs
    # the same run can be selected with different filters
    sel_runs = {}
    for name, info in rctrl.getWorkflows().items():
        # get runs from OMS
        runs_data = rr.get_runs(filters=json.loads(info['rr_filter']),
                                min_run=rrange[0],
                                max_run=rrange[1],
                                daq_completed=True)
        for r in runs_data:
            # new run
            if r not in sel_runs:
                sel_runs[r] = runs_data[r]
                sel_runs[r]['workflows'] = {}

            sel_runs[r]['workflows'].update({wf: 'new' for wf in info['active']})

    for r in sorted(sel_runs.keys())[:n_runs]:
        # set T0 acquisition era (unless it's overrided)
        resp = requests.get(
            url=f'https://cmsweb.cern.ch/t0wmadatasvc/prod/run_config?run={r}',
            verify=False).json()['result']
        if len(resp):
            acq_era = resp[-1]['acq_era']
        else:
            # if acq_era not available for the current run get the value from the
            # current run under processing in T0
            acq_era = requests.get(
                url='https://cmsweb.cern.ch/t0wmadatasvc/prod/run_config',
                verify=False).json()['result'][-1]['acq_era']
        if globaltag == 'hlt':
            # get  HLT global tag from OMS
            oms = QueryOMS()
            gt = oms.get_hlt_gt(run=r)
            # if OMS record is empty get GT from previous run
            if gt == '':
                gt = rctrl.getStatus(run=rctrl.getLastRun())[-1]['globaltag']
        else:
            # get global tag from T0 reco config
            t0api = T0GeneralAPI()
            gt = t0api.get_gt(run=r, rtype=globaltag)

        if gt != '':
            if rctrl.injectRun(r,
                               run_endtime=sel_runs[r]['end_time'],
                               acq_era=acq_era,
                               gt=gt,
                               fill=sel_runs[r]['fill_number'],
                               status=sel_runs[r]['workflows'],
                               lumi=sel_runs[r]['recorded_lumi']):
                logging.info('Run %s injected for campaign %s in the %s db as new for these workflows: %s', str(r), campaign, dbname, ','.join(sel_runs[r]['workflows'].keys()))
            else:
                logging.error('Run %s not injected as new for campaign %s in %s. Influxdb not reachable.', str(r), campaign, dbname)
        else:
            logging.warning('Run %s not injected as new for campaign %s in %s. No global tag information found.', str(r), campaign, dbname)        
                
    return 0


def list_campaigns(dbname: str,
                   quite: bool = False,
                   minimal: bool = False,
                   **kwargs) -> List[str]:
    """
    List the campaigns inside the database.

    :param dbname: database name.
    :param quite: do not print the list of campaigns.
    :return: list of campaigns.
    """
    db = InfluxDBClient(host=dbhost,
                        port=dbport,
                        username=dbusr,
                        password=dbpwd,
                        ssl=dbssl,
                        database=dbname,
                        timeout=30_000)
    campaigns = []
    res = db.query(f'SHOW TAG VALUES ON "{dbname}" FROM "workflows" WITH KEY = "campaign"')
    for c in res.get_points():
        campaigns.append(c['value'])

    # log if required
    if not quite:
        if minimal:
            print('\n'.join(campaigns))
        else:
            logging.info('Available campaigns: \n- '+'\n- '.join(campaigns))
        return 0
    else:
        return campaigns


def create_campaign(dbname: str,
                    campaign: str,
                    clone: str = None,
                    **kwargs):
    """
    Create a new reprocessing campaign.

    This function only setup the database for a new campaign. The actual
    data processing is submitted using :func:`~inject_campaign`.
    
    :param dbname: name of the database istance.
    :param campaign: name of the new campaign (abort if campaign exist).
    :param clone: the name of an existing campaign from which to clone the list of run types and active workflows. If not provided, run types should be created using the specific command.
    """
    if campaign in list_campaigns(dbname, quite=True):
        logging.warning(f'Campaign {campaign} already exists. Please specify a different name.')
        return 0

    rctrl = RunCtrl(dbname=dbname, campaign=campaign)    
    if clone != None:
        # get clone rtypes
        rctrl_clone = RunCtrl(dbname=dbname, campaign=clone)
        rtypes = rctrl_clone.getWorkflows()
    else:
        # basic defaults
        rtypes = {'all': {
            'rr_filter': '[{"attribute_name":"sequence", "operator":"EQ", "value":"GLOBAL-RUN"}]',
            'active': None}}

    # create rtypes for new campaign
    for tag, rt in rtypes.items():
        rctrl.createRunType(tag=tag,
                            rr_filter=rt['rr_filter'],
                            add=rt['active'])


def inject_campaign(dbname: str,
                    campaign: str,
                    era: str,
                    globaltag: str,
                    rrange: Optional[List[int]] = None,
                    runs: Optional[List[int]] = None,
                    fills: Optional[List[int]] = None,
                    certjson: Optional[str] = None,
                    **kwargs):
    """
    Inject a list of runs/fills or entire era into a new campaign.

    The campaign must be created before hand using :func:`~create_campaign`.
    
    :param dbname: name of the database istance.    
    :param campaign: name of the new campaign (abort if campaign exist).
    :param era: CMS data acquisition era.
    :param globaltag: GT name to be used by all workflows.
    :param rrange: run range.
    :param runs: list of run numbers.
    :param fills: list of LHC fills.
    :param certjson: path to JSON file with certified runs.
    """
    rr = QueryOMS()
    rctrl = RunCtrl(dbname=dbname, campaign=campaign)

    if campaign not in list_campaigns(dbname, quite=True):
        logging.warning(f'Campaign {campaign} does not exist. Please create it first')
        return 0
    
    if runs or fills:
        rrange = [None, None]
    else:
        rrange.sort()

    if len(rrange) != 2:
        logging.error(f'--range requires exactly 2 values (comma separated). {len(rrange)} provided.')

    # create the list of runs to be processed, take into account the certification json
    runs = set(runs) if runs else set([])
    if certjson != None:
        with open(certjson) as jf:
            certruns = [int(r) for r in list(json.load(jf))]

    # get workflows and runs
    # the same run can be selected with different filters
    sel_runs = {}
    for name, info in rctrl.getWorkflows().items():
        # get runs from OMS filtering by era
        sel = json.loads(info['rr_filter'])
        #sel.append({"attribute_name": "era", "operator": "EQ", "value": era})
        runs_data = rr.get_runs(filters=sel,
                                runs=list(runs),
                                fills=fills,
                                min_run=rrange[0],
                                max_run=rrange[1],
                                daq_completed=True)
        for r in runs_data:
            # run only on cert json runs
            if certjson != None and r not in certruns:
                continue
            # new run
            if r not in sel_runs:
                sel_runs[r] = runs_data[r]
                sel_runs[r]['workflows'] = {}

            sel_runs[r]['workflows'].update({wf: 'new' for wf in info['active']})

    for r in sorted(sel_runs.keys()):
        if rctrl.injectRun(r,
                           run_endtime=sel_runs[r]['end_time'],
                           acq_era=era,
                           gt=globaltag,
                           certjson=certjson,
                           fill=sel_runs[r]['fill_number'],
                           status=sel_runs[r]['workflows'],
                           lumi=sel_runs[r]['recorded_lumi']):
            logging.info('Run %s injected in the %s db for reprocessing. Injected workflows: %s', str(r), dbname, ','.join(sel_runs[r]['workflows'].keys()))

        else:
            logging.error('Run %s not injected as new in %s. Influxdb not reachable.', str(r), dbname)

    return 0

def inject_campaign_mc(dbname: str,
                       campaign: str,
                       era: str,
                       globaltag: str,
                       rrange: Optional[List[int]] = None,
                       runs: Optional[List[int]] = None,
                       **kwargs):
    """
    Inject a list of runs into a new MC campaign.

    The campaign must be created before hand using :func:`~create_campaign`.
    
    :param dbname: name of the database istance.    
    :param campaign: name of the new campaign (abort if campaign exist).
    :param era: CMS data acquisition era.
    :param globaltag: GT name to be used by all workflows.
    :param rrange: run range.
    :param runs: list of run numbers.
    """
    rctrl = RunCtrl(dbname=dbname, campaign=campaign)

    if campaign not in list_campaigns(dbname, quite=True):
        logging.warning(f'Campaign {campaign} does not exist. Please create it first')
        return 0
    
    if runs:
        rrange = [None, None]
    else:
        rrange.sort()

    if len(rrange) != 2:
        logging.error(f'--range requires exactly 2 values (comma separated). {len(rrange)} provided.')

    # create the list of runs to be processed, take into account the certification json
    runs = set(runs) if runs else set([])

    # get workflows and runs
    # the same run can be selected with different filters
    sel_runs = {}
    for name, info in rctrl.getWorkflows().items():
        for r in runs:
            # new run
            if r not in sel_runs:
                sel_runs[r] = {'workflows': {}}
            sel_runs[r]['workflows'].update({wf: 'new' for wf in info['active']})

    for r in sorted(sel_runs.keys()):
        if rctrl.injectRun(r,
                           run_endtime=datetime.utcnow().strftime("%Y-%m-%dT%H:%M:%SZ"),
                           acq_era=era,
                           gt=globaltag,
                           certjson='',
                           fill=0,
                           status=sel_runs[r]['workflows'],
                           lumi=0):
            logging.info('Run %s injected in the %s db for reprocessing. Injected workflows: %s', str(r), dbname, ','.join(sel_runs[r]['workflows'].keys()))

        else:
            logging.error('Run %s not injected as new in %s. Influxdb not reachable.', str(r), dbname)

    return 0


def create_rtype(dbname: str = 'test',
                 campaign: str = None,
                 rtype: str = None,
                 rr_filter: str = None,
                 wflows_add: List[str] = None,
                 from_type: str = None,
                 **kwargs):
    """
    Create a new run type with its workflow list.

    :param dbname: database name.
    :param rr_filter: OMS query filters.
    :param rtype: run type name.
    :param wflows_add: list of worflows to add.
    :param from_type: a run type to copy from.
    """
    rctrl = RunCtrl(dbname=dbname, campaign=campaign)
    rctrl.createRunType(tag=rtype,
                        rr_filter=rr_filter,
                        add=wflows_add.split(','),
                        from_tag=from_type)

    return 0


def list_wflows(dbname: str = None,
                campaign: str = None,
                rtype: str = None,                
                **kwargs):
    """
    List run types.

    :param dbname: database name.
    :param campaign: processing campaign.
    :param rtype: run type name.
    """
    rctrl = RunCtrl(dbname=dbname, campaign=campaign)
    logging.info(pformat(rctrl.getWorkflows(rtype)))

    
def update_wflows(dbname: str = None,
                  campaign: str = None,
                  rtype: str = None,
                  wflows_add: str = None,
                  wflows_remove: str = None,
                  rr_filter: str = None,
                  **kwargs):
    """
    Update the list of active workflows.

    :param dbname: database name.
    :param campaign: processing campaign.
    :param rtype: run type name.
    :param wflows_add: list of worflows to add.
    :param wflows_remove: list of worflows to remove.
    :param rr_filter: OMS query filters.
    """
    if not wflows_add and not wflows_remove and not rr_filter:
        logging.error('Please specify at least one among --add, --remove or --rr-filter')
        return -1

    rctrl = RunCtrl(dbname=dbname, campaign=campaign)
    rctrl.updateWorkflows(tag=rtype,
                          add=wflows_add.split(','),
                          remove=wflows_remove.split(','),
                          rr_filter=rr_filter)
    logging.info(rctrl.getWorkflows(rtype))

    return 0

def status(dbname: str = 'test',
           campaign: str = 'prompt',
           reply: str = None,
           runs: List[int] = None,
           **kwargs):
    """
    Get status of specified runs

    :param dbname: database name.
    :param campaign: processing campaign.
    """
    rctrl = RunCtrl(dbname=dbname, campaign=campaign)
    
    result = []
    formatted_result = ""
    title = False
    for run in runs:
        query = rctrl.getStatus(run)
        result.append(query[0])

    for elem in result:
        title_row, sep_row, data_row = "|", "|", "|"
        keys = iter(elem.keys())
        for i in range(len(elem)):
            key = next(keys)
            title_row += f" {key} |"
            sep_row += " :" + "-"*(len(str(elem[key]))+2) + ": |"
            if type(elem[key]) is float:
                # Round to 3 decimal places if value is number
                data_row += " {:.2f} |".format(elem[key])
            elif key == "time" or key == "endtime":
                # Remove T and Z from middle and end of datetime string
                data_row += " {0} |".format(elem[key][:10] + " " + elem[key][11:-1])
            else:
                data_row += f" {elem[key]} |"

        if not title:
            formatted_result += title_row + '\n' + sep_row + '\n' + data_row + '\n'
            title = True
        else:
            formatted_result += data_row + '\n'

    logging.info("Formatted result generated.")
    logging.info(formatted_result)

    if reply:
        data = {
            'attachments': [{'text': formatted_result}]
        }

        requests.post(reply, json=data)

    return 0

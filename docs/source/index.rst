.. ECAL automation control documentation master file, created by
   sphinx-quickstart on Fri Mar 12 10:19:56 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.
   
Welcome to ECAL automation control's documentation!
===================================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

Installation
============

The package can be installed through pip:

.. code-block:: bash

   python -m pip install 'https://gitlab.cern.ch/cms-ecal-dpg/ECALELFS/automation-control.git'

Note that in most cases the :code:`--user` option is needed in order to install without having an admin accout (i.e. on lxplus). The pip installation works also
to install the package in a CMSSW environment. After calling :code:`cmsenv`, `ecalautoctrl` 
can be installed using:

.. code-block:: bash
                
   python3 -m pip install 'https://gitlab.cern.ch/cms-ecal-dpg/ECALELFS/automation-control.git'
   cp ~/.local/bin/ecal* $CMSSW_BASE/bin/$SCRAM_ARCH/

The last line copies the executable scripts into a directory under the `PATH` set by `cmsenv`.
This last step should be eventually replaced once a proper option to specify the scripts
installation path becomes available in `pip`.

It is not required to have a CMSSW release to use the `ecalautoctrl` package.
Creating a conda environment to install the package locally is highly recommended:

.. code-block:: bash

   conda create -n py39-ectrl python==3.9
   conda activate py39-ectrl
   python -m pip install 'https://gitlab.cern.ch/cms-ecal-dpg/ECALELFS/automation-control.git'
   
Note that some of the package dependencies (`dbs3-client`) requires libraries
that are not automatically installed by `pip` but have to be installed manually using the
OS package manager.
   
Known installation issues
-------------------------

The `dbs3-client` package requires `pycurl`. The `pycurl` package itself does not install
the curl libraries by default, these have to be manually installed using the OS package manager.
For Redhat based distribution the libraries are installed using:

.. code-block:: bash

   yum install libcurl-devel

On Debian based:

.. code-block:: bash

   apt-get install libcurl-devel

On Arch:

.. code-block:: bash

   apt-get install libcurl-compat
   
The `libcurl` library should be installed before the `ecalautoctrl` package.
When installing inside a conda environment a runtime an error, related to the openssl.1.1 library, might be raised. The error is fixed by removing the spurious library file from the conda environment:

.. code-block:: bash

   find -name libcrypto.so.1.1 -delete -print

   
ECAL ecalautomation.py script
=============================
.. argparse::
   :filename: ../bin/ecalautomation.py
   :func: cmdline_options
   :prog: ecalautomation.py

ECAL ecalrunctrl.py script
==========================
.. argparse::
   :filename: ../bin/ecalrunctrl.py
   :func: cmdline_options
   :prog: ecalrunctrl.py

ECAL copysts.py script
==========================
.. argparse::
   :filename: ../bin/copysts.py
   :func: cmdline_options
   :prog: copysts.py
          
TaskHandlers
============

.. autoclass:: ecalautoctrl.TaskHandlers.AutoCtrlScriptBase
   :members:
   :special-members: __call__    
      
.. autoclass:: ecalautoctrl.HandlerBase
   :members:

.. autoclass:: ecalautoctrl.HTCHandler
   :members:

.. autoclass:: ecalautoctrl.HTCHandlerByRun
   :members:
   :show-inheritance:
   :inherited-members: ecalautoctrl.HTCHandler
   :exclude-members: cmdline_options, check_running_job                     
      
.. autoclass:: ecalautoctrl.HTCHandlerByRunDBS
   :members:
   :show-inheritance:
   :inherited-members: ecalautoctrl.HTCHandler
   :exclude-members: cmdline_options, check_running_job                     

.. autoclass:: ecalautoctrl.HTCHandlerByFill
   :members:
   :show-inheritance:
   :inherited-members: ecalautoctrl.HTCHandler
   :exclude-members: cmdline_options, check_running_job                     

.. autoclass:: ecalautoctrl.HTCHandlerGrowingFill
   :members:
   :show-inheritance:
   :inherited-members: ecalautoctrl.HTCHandler
   :exclude-members: cmdline_options, check_running_job                     

Data access decorators
----------------------
.. autofunction:: ecalautoctrl.dbs_data_source

.. autofunction:: ecalautoctrl.prev_task_data_source

Grouping decorators
-------------------
.. autofunction:: ecalautoctrl.process_by_run

.. autofunction:: ecalautoctrl.process_by_fill

.. autofunction:: ecalautoctrl.process_by_era

.. autofunction:: ecalautoctrl.process_by_intlumi

.. autofunction:: ecalautoctrl.process_by_nevents
           
Locks
=====

Lock base
---------
.. autoclass:: ecalautoctrl.TaskHandlers.LockBase
   :members:

Tier0 locks
-----------
.. autoclass:: ecalautoctrl.tier0_locks.T0ProcDatasetLock
   :members:
   :show-inheritance:

CondDB locks
------------
.. autoclass:: ecalautoctrl.conddb_api.CondDBLockBase
   :members:
   :show-inheritance:
   
.. autoclass:: ecalautoctrl.conddb_api.CondDBLockGT
   :members:
   :show-inheritance:
   :inherited-members: ecalautoctrl.conddb_api.CondDBLockBase

.. autoclass:: ecalautoctrl.conddb_api.CondDBLockTags
   :members:
   :show-inheritance:
   :inherited-members: ecalautoctrl.conddb_api.CondDBLockBase
                  
CMS tools
=========
.. autoclass:: ecalautoctrl.QueryOMS
   :members:

.. autoclass:: ecalautoctrl.QueryDBS
   :members:

RunCtrl
=======
.. autoclass:: ecalautoctrl.RunCtrl
   :members:      
      
JobCtrl
=======
.. autoclass:: ecalautoctrl.JobCtrl
   :members:

.. autoclass:: ecalautoctrl.JobStatus
   :members:
      
Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

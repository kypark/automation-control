# ECAL automation-control package
  This package provide all the utilities necessary to run the ECAL Automation workflows with the influxdb backend:
  - Update job status to the db
  - Control job progress

## Requirements
  This package works only with python3

## Installation
  ```
  pip install "git+https://gitlab.cern.ch/cms-ecal-dpg/ECALELFS/automation-control.git"
  ```
  
## Usage
  - (Online documentation)[https://cmsecaldocs.web.cern.ch/cmsecaldocs/ecalautoctrl/]
  - for the =ecalautomation.py= script you may also refer to the script help:
    `ecalautomation.py --help`
